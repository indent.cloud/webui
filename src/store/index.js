import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'
import organization from './organization'
import auth from './auth'
import issues from './issues/index'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    organization,
    auth,
    issues
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
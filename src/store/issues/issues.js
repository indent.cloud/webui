import Vue from 'vue';
import ApiService from '@/services/API.js'

// initial state
const state = {
  list: {},
  detail: {}
}

// getters
const getters = {
  getIssuesByProjectIdentifier: (state) => (identifier) => {
    let list = state.list[identifier];

    if(list) {
      return list;
    } else {
      return [];
    }
  },
  getIssueByKey: (state) => (key) => {
    let list = state.detail[key];

    if(list) {
      return list;
    } else {
      return {};
    }
  }
}

// actions
const actions = {
  getIssuesListForProject({ commit }, identifier) {
    ApiService.get('/api/issues/v1/projects/' + identifier + '/issues')
      .then(resp => {
        const issues = resp.data;
        commit('setIssuesList', {identifier, issues});
      })
      .catch(err => {
        console.log('Failed to get issues', err);
      }
    );
  },
  getIssueByKey({ commit }, key) {
    ApiService.get('/api/issues/v1/issues/' + key)
      .then(resp => {
        const details = resp.data;
        commit('setIssueDetails', {key, details});
      })
      .catch(err => {
        console.log('Failed to get issue ' + key, err);
      }
    );
  },
  createNew({ commit }, issue_data) {
    return new Promise((resolve, reject) => {
      ApiService.post('/api/issues/v1/issues/', issue_data)
        .then(resp => {
          const details = resp.data;

          resolve(details)
        })
        .catch(err => {
          console.log('Failed to create new issue ', err);
          reject(err);
        }
      )
    })
  },
  updateIssueByKey({ commit }, {key, issue_data}) {
    return new Promise((resolve, reject) => {
      ApiService.patch('/api/issues/v1/issues/' + key, issue_data)
        .then(resp => {
          const response_data = resp.data;
          if (response_data.success) {
            const details = response_data.issue;
            commit('setIssueDetails', {key, details});
          } else {
            reject(response_data);
          }

          resolve();
        })
        .catch(err => {
          console.log('Failed to get issue ' + key, err);
          reject(err);
        }
      )
    })
  }
}

// mutations
const mutations = {
  setIssuesList(state, {identifier, issues}) {
    Vue.set(state.list, identifier, issues);
  },
  setIssueDetails(state, {key, details}) {
    Vue.set(state.detail, key, details);
  },
  updateIssueDetails(state, {key, details}) {
    const issue = state.detail[key];
    const updatedIssue = Object.assign(issue, details);

    Vue.set(state.detail, key, updatedIssue);
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
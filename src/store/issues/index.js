import projects from './projects'
import issues from './issues'

const state = {}
const getters = {}
const actions = {}
const mutations = {}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: {
    projects,
    issues
  }
}
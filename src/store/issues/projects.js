import Vue from 'vue';
import ApiService from '@/services/API.js'

// initial state
const state = {
  all: [],
  details: {}
}

function getIdentifier(params) {
  if (params.identifier) {
    return params.identifier;
  } else if (params.key) {
    return params.key.split('-')[0];
  } else {
    throw 'Cannot derive identifier';
  }
}

// getters
const getters = {
  getIdentifierByParams: () => (params) => {
    let identifier = getIdentifier(params);

    return identifier;
  },
  getProjectDetailsByIdentifier: (state) => (identifier) => {
    let detail = state.details[identifier];

    if(detail) {
      return detail;
    } else {
      return {};
    }
  },
  getProjectDetailsByParams: (_, getters) => (params) => {
    let identifier = getIdentifier(params);

    return getters.getProjectDetailsByIdentifier(identifier);
  }
}

// actions
const actions = {
  getAll({ commit }) {
    ApiService.get('/api/issues/v1/projects')
      .then(resp => {
        commit('setProjects', resp.data)
      })
      .catch(err => {
        console.log('Failed to get projects', err)
      }
    );
  },
  getProject({ commit }, identifier) {
    ApiService.get('/api/issues/v1/projects/' + identifier)
      .then(resp => {
        const detail = resp.data;
        commit('setProjectDetails', {identifier, detail});
      })
      .catch(err => {
        console.log('Failed to get projects', err);
      }
    );
  },
  getProjectByParams({ commit, dispatch }, params) {
    let identifier = getIdentifier(params);

    return dispatch('getProject', identifier);
  }
}

// mutations
const mutations = {
  setProjects(state, all) {
    state.all = all;
  },
  setProjectDetails(state, {identifier, detail}) {
    // state.details[identifier] = detail;
    Vue.set(state.details, identifier, detail);
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
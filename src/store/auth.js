import axios from 'axios'
import ApiService from '@/services/API';

// initial state
const state = {
  status: 'uninitialized',
  user : {}
}

// getters
const getters = {}

// actions
const actions = {
  login({commit}, user) {
    return new Promise((resolve, reject) => {
      commit('auth_request')
      ApiService.post('/api/auth/login', user)
        .then(resp => {
          const user = resp.data.user;

          commit('auth_success', {user});
          resolve(resp);
        })
        .catch(err => {
          commit('auth_error');

          if (err.response.data) {
            err = err.response.data.message;
          }

          reject(err)
        }
      );
    })
  },
  logout({commit}) {
    return new Promise((resolve, reject) => {
      ApiService.delete('/api/auth/logout')
        .then(resp => {
          commit('logout');
          resolve(resp);
        })
        .catch(err => {
          console.log(err);

          commit('logout');
          reject(err);
        }
      );
    })
  },
  getUser({commit}) {
    return new Promise((resolve, reject) => {
      commit('auth_request')
      axios({url: '/api/auth/whoami', method: 'GET' })
      .then(resp => {
        const user = resp.data;

        commit('set_user', user);
        resolve(resp);
      })
      .catch(err => {
        if (err.response.data) {
          err = err.response.data.message;
        }

        reject(err)
      })
    })
  }
}

// mutations
const mutations = {
  auth_request(state){
    state.status = 'loading';
  },
  auth_success(state, {user}) {
    state.status = 'success';
    state.user = user;
  },
  auth_error(state) {
    state.status = 'error';
  },
  set_user(state, user) {
    state.user = user;
  },
  logout(state) {
    state.status = '';
    state.user = {};
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
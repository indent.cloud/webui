import ApiService from '@/services/API.js'

// initial state
const state = {
  info: {}
}

// getters
const getters = {}

// actions
const actions = {
  getOrganization ({ commit }) {
    ApiService.get('/api/organization/v1/info')
      .then(resp => {
        commit('setOrganization', resp.data)
      })
      .catch(err => {
        console.log('Failed to get organization', err)
      }
    );
  }
}

// mutations
const mutations = {
  setOrganization (state, info) {
    state.info = info
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ApiService from "./services/API";
import axios from 'axios'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import VueBootstrapTypeahead from 'vue-bootstrap-typeahead'

import { BVToastPlugin } from 'bootstrap-vue'

Vue.use(BVToastPlugin)
Vue.component('vue-bootstrap-typeahead', VueBootstrapTypeahead)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

ApiService.init();

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

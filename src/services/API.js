import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import router from '@/router'

const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
  },

  query(resource, params) {
    return Vue.axios.get(resource, params).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },

  request(request) {
    return new Promise((resolve, reject) => {
      axios(request)
        .then(resp => resolve(resp))
        .catch(err => {
          if (err.response.data.error) {
            console.log(router.currentRoute);
            if (err.response.data.error === 'Auth/NotAuthenticated' 
                && router.currentRoute.name != 'Login'
                && router.currentRoute.matched[0].path != '/error'
            ) {
              router.push('/login');
            }

            if (err.response.data.error === 'Organization/NotFound' && router.currentRoute.name != 'OrganizationNotFound') {
              router.push('/error/organizationnotfound');
            }
          }

          reject(err)
        })
    });
  },

  get(resource) {
    return this.request({url: resource, method: 'GET' });
  },

  post(resource, data) {
    return this.request({url: resource, data: data, method: 'POST' });
  },

  patch(resource, data) {
    return this.request({url: resource, data: data, method: 'PATCH' });
  },

  update(resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params);
  },
  
  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },

  delete(resource) {
    return this.request({url: resource, method: 'DELETE' });
  },
};

export default ApiService;
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: { name: 'OrganizationDashboard' }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "Login" */ '@/views/Auth/Login.vue')
  },
  {
    path: '/logout',
    name: 'Logout',
    component: () => import(/* webpackChunkName: "Logout" */ '@/views/Auth/Logout.vue')
  },
  {
    path: '/organization',
    component: () => import('@/views/Organization/Entrypoint.vue'),
    children: [
      {
        name: 'OrganizationDashboard',
        path: '',
        props: { sidebarActiveKey: 'dashboard' },
        component: () => import('@/views/Organization/Dashboard.vue')
      }
    ]
  },
  {
    path: '/issues',
    component: () => import('@/views/Issues/Entrypoint.vue'),
    children: [
      {
        name: 'IssuesHomeDashboard',
        path: '',
        component: () => import('@/views/Issues/Home.vue')
      },
      {
        path: 'view',
        props: { sidebarActiveKey: 'list' },
        component: () => import('@/views/Issues/ProjectView.vue'),
        children: [
          {
            name: 'IssuesProjectViewIssue',
            path: ':key',
            props: { sidebarActiveKey: 'list' },
            component: () => import('@/views/Issues/View.vue')
          }
        ]
      },
      {
        path: 'projects/:identifier',
        component: () => import('@/views/Issues/ProjectView.vue'),
        children: [
          {
            name: 'IssuesProjectBoards',
            path: 'boards',
            props: { sidebarActiveKey: 'boards' },
            component: () => import('@/views/Issues/Boards.vue')
          },
          {
            name: 'IssuesProjectIssuesList',
            path: 'list',
            props: { sidebarActiveKey: 'list' },
            component: () => import('@/views/Issues/IssuesList.vue')
          },
          {
            name: 'IssuesProjectReleases',
            path: 'releases',
            props: { sidebarActiveKey: 'releases' },
            component: () => import('@/views/Issues/Releases.vue')
          },
          {
            name: 'IssuesProjectRoadmap',
            path: 'roadmap',
            props: { sidebarActiveKey: 'roadmap' },
            component: () => import('@/views/Issues/Roadmap.vue')
          },
          {
            name: 'IssuesProjectReports',
            path: 'reports',
            props: { sidebarActiveKey: 'reports' },
            component: () => import('@/views/Issues/Reports.vue')
          },
          {
            name: 'IssuesProjectSettings',
            path: 'settings',
            props: { sidebarActiveKey: 'settings' },
            component: () => import('@/views/Issues/ProjectSettings.vue')
          },
          // {
          //   name: 'IssuesProjectViewIssue',
          //   path: 'view/:key',
          //   props: { sidebarActiveKey: 'list' },
          //   component: () => import('@/views/Issues/View.vue')
          // },
        ]
      }
    ]
  },
  {
    path: '/docs',
    name: 'Docs',
    props: { statusVariant: 'warning', statusText: 'No estimate currently' },
    component: () => import('@/views/WorkInProgress.vue')
  },
  {
    path: '/code',
    name: 'Code',
    props: { statusVariant: 'warning', statusText: 'No estimate currently (Phase 2)' },
    component: () => import('@/views/WorkInProgress.vue')
  },
  {
    path: '/pipelines',
    name: 'Pipelines',
    props: { statusVariant: 'warning', statusText: 'No estimate currently (Phase 4)' },
    component: () => import('@/views/WorkInProgress.vue')
  },
  {
    path: '/artifacts',
    name: 'Artifacts',
    props: { statusVariant: 'warning', statusText: 'No estimate currently (Phase 3)' },
    component: () => import('@/views/WorkInProgress.vue')
  },
  {
    path: '/security',
    name: 'Security',
    props: { statusVariant: 'warning', statusText: 'No estimate currently' },
    component: () => import('@/views/WorkInProgress.vue')
  },
  {
    path: '/error',
    component: () => import('@/views/Error/entrypoint.vue'),
    children: [
      {
        name: 'ErrorOrganizationNotFound',
        path: 'organizationnotfound',
        component: () => import('@/views/Error/OrganizationNotFound.vue')
      }
    ]
  },
  {
    path: '*',
    name: 'NotFound',
    component: () => import('@/views/Error/404.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
